const express = require('express');
const morgan = require('morgan');
const app = express();
const filesRouter = require('./router');
app.use(morgan('combined'));
app.use(express.json());
app.use('/api/files', filesRouter)
app.listen(8080, () => console.log("Server is running on port", 8080));

