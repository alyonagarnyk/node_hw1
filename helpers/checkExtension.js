function checkExtension(filename) {
	const EXTENSIONS = ["log", "txt", "json", "yaml", "xml", "js"];
	const fileExtension = filename.slice(filename.lastIndexOf(".") + 1);
	return EXTENSIONS.find((extension) => extension === fileExtension);
}

module.exports = checkExtension;
