const express = require('express');
const filesRouter = express.Router();

const {createFile, getFiles, getFile} = require('./controllers');

filesRouter.post('/', createFile);
filesRouter.get('/', getFiles);
filesRouter.get('/:filename', getFile);
// contactsRouter.post('/', addContact);
// contactsRouter.delete('/:contactId', removeContact);
// contactsRouter.patch('/:contactId', updateContact);

module.exports = filesRouter;
