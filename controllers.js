const fsPromises = require("fs").promises;
const path = require("path");
const dataValidator = require("./helpers/dataValidator");
const checkExtension = require("./helpers/checkExtension");

function createFile(req, res) {
	const result = dataValidator(req, res);
	if (result.error) {
		res.status(400).json({ message: `Please specify ${result.error.details[0].context.key} parameter` });
	}
	const isCorrectExtension = checkExtension(req.body.filename);
	if (!isCorrectExtension) {
		res.status(400).json({ message: "Sorry, application support just log, txt, json, yaml, xml, js file extensions" });
	}
	return fsPromises
		.writeFile(path.join(__dirname, "./", "files", req.body.filename), req.body.content, "utf8")
		.then(res.status(200).json({ message: "File created successfully" }))
		.catch((error) => {
			res.status(500);
			throw error;
		});
}

function getFiles(req, res) {
	return fsPromises
		.readdir(path.join(__dirname, "./", "files"))
		.then((data) => {
			if (!data || !data.length) {
				res.status(400).json({ message: "Client error" });
			}
			res.status(200).json({ message: "Success", files: data });
		})
		.catch((error) => {
			res.status(500);
			throw error;
		});
}

function getFile(req, res) {
	let fileInfo;
	return fsPromises
		.readdir(path.join(__dirname, "./", "files"))
		.then((data) => {
			const file = data.find((filename) => filename === req.params.filename);
			if (!file) {
				res.status(400).json({ message: `No file with ${req.params.filename} filename found` });
			}
			return fsPromises.readFile(path.join(__dirname, "./", "files", req.params.filename), "utf8");
		})
		.then((data) => {
			fileInfo = {
				message: "Success",
				filename: req.params.filename,
				content: data,
				extension: req.params.filename.slice(req.params.filename.lastIndexOf(".") + 1),
			};
			return fileInfo;
		})
		.then(() => fsPromises.stat(path.join(__dirname, "./", "files", req.params.filename)))
		.then((data) => res.status(200).json({ ...fileInfo, uploadedDate: data.birthtime }))
		.catch((error) => {
			res.status(500);
			throw error;
		});
}

module.exports = {
	createFile,
	getFiles,
	getFile,
};
