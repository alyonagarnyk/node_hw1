const Joi = require('joi');

function dataValidator(req, res) {
    const schema = Joi.object({
        filename: Joi.string().required(),
        content: Joi.string().required()
      });
    const result = schema.validate(req.body);
    return result
}

module.exports = dataValidator;